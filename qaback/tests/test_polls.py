import pytest
import sys

sys.path.insert(1, './pages')
from selenium import webdriver
from pages.pages import HomePage, ResultsPage
from pages.locators import *

import time


@pytest.fixture
def driver():
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--start-maximized')
    chrome_options.add_argument('--ignore-certificate-errors')
    # chrome_options.add_argument('--lang=en')
    driver = webdriver.Chrome(options=chrome_options)
    yield driver
    driver.close()


@pytest.mark.parametrize("choice", [1, 2, 3])
def test_vote_and_check_results(driver, choice):
    results = ResultsPage(driver)
    print("Get initial results")
    initial_results = results.get_result_labels()
    expected_results = initial_results.copy()
    expected_results[choice - 1] += 1

    home = HomePage(driver)
    home.vote_by_index(choice)

    print("Get final results")
    final_results = results.get_result_labels()
    results.assert_vote_again()

    print("Assert expected results")
    assert final_results == expected_results
