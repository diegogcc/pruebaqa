from selenium.webdriver.common.by import By


class HomeLocators:
    VOTE_BUTTON = (By.CSS_SELECTOR, "[type='submit']")
    RADIOS = (By.ID, "choice%s")


class ResultsLocators:
    RESULT_LABELS = (By.CSS_SELECTOR, "ul > li:nth-child(%s)")
    VOTE_BUTTON = (By.XPATH, "//a[contains(text(), 'Vote')]")
