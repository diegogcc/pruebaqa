import os

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from .locators import *


class BasePage:
    """
    BasePage class will hold common elements and functionalities
    """

    def __init__(self, driver):
        self.driver = driver
        self.wait = WebDriverWait(self.driver, 10)

    def click(self, by_locator):
        self.wait.until(EC.visibility_of_element_located(by_locator)).click()

    def read_element_text(self, by_locator):
        element = self.wait.until(EC.visibility_of_element_located(by_locator))
        return element.text

    def assert_element_text_contains(self, by_locator, expected):
        element = self.wait.until(EC.visibility_of_element_located(by_locator))
        assert expected.lower() in str(element.text).lower()

    def assert_element_text_matches(self, by_locator, expected):
        element = self.wait.until(EC.visibility_of_element_located(by_locator))
        assert element.text == expected

    def is_element_enabled(self, by_locator):
        element = self.wait.until(EC.visibility_of_element_located(by_locator))
        return element.is_enabled()

    def is_element_checked(self, by_locator):
        element = self.wait.until(EC.presence_of_element_located(by_locator))
        checked = element.get_attribute('checked')
        return checked == 'true'  # JS boolean 'true' and not 'True'

    def count_elements(self, by_locator):
        elements = self.wait.until(EC.visibility_of_all_elements_located(by_locator))
        return len(elements)

    def assert_current_url(self, expected):
        assert self.driver.current_url == expected

    def take_page_screenshot(self, directory, basename):
        file_name = os.path.join(directory, basename + '.png')
        self.driver.save_screenshot(file_name)
        print("Screenshot taken and saved in: {}".format(file_name))
        return file_name


class HomePage(BasePage):
    def __init__(self, driver):
        super().__init__(driver)
        self.driver.get("http://127.0.0.1:8000/polls/1/")

    def vote_by_index(self, index):
        """Selects the radio button according to the index and votes for it.
        :param index:   radio choice (starts at 1, not 0)
        """
        print("\nVoting for option: {}".format(index))
        print('-' * 20)
        radio_selector = HomeLocators.RADIOS
        radio_selector = (radio_selector[0], radio_selector[1] % str(index))
        self.click(radio_selector)
        self.click(HomeLocators.VOTE_BUTTON)


class ResultsPage(BasePage):
    def __init__(self, driver):
        super().__init__(driver)
        self.driver.get("http://127.0.0.1:8000/polls/1/results/")

    @staticmethod
    def _extract_votes_number(text):
        print("Extracting votes from '{}'".format(text))
        return int((text.split(' -- ')[1]).split()[0])

    def get_result_labels(self):
        result_list = []
        for i in range(1, 4):
            result_element = ResultsLocators.RESULT_LABELS
            result_element = (result_element[0], result_element[1] % str(i))
            text = self.read_element_text(result_element)
            result_list.append(self._extract_votes_number(text))
        return result_list

    def assert_vote_again(self):
        self.click(ResultsLocators.VOTE_BUTTON)
        self.assert_current_url("http://127.0.0.1:8000/polls/1/")

